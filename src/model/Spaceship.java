package model;

public class Spaceship extends Unit {

	public static final double SPEED = .01;
	private Point target;

	public Spaceship(int team, Point position) {
		super(team, position);
		target = position;
	}

	public void spreadShips() {

	}

	public void destroyShip() {

	}

	public void selectShips() {

	}

	public void deselectShips() {

	}

	public void goToPosition(Point target) {
		this.target = target;
	}

	@Override
	public void update(double deltaT) {
//		System.out.println("deltaT: "+deltaT);
//		System.out.println("position "+getPosition().toString());
//		System.out.println("target "+target.toString());
		if (getPosition() != target) {
			double dist = Math.hypot(
					getPosition().getX() - target.getX(),
					getPosition().getY() - target.getY());
			double step = deltaT * SPEED;
//			System.out.println("dist: "+dist);
//			System.out.println("SPEED: "+SPEED+"\n");
			if (dist < step)
				setPosition(target);
			else
				setPosition(getPosition().add(target.sub(getPosition()).normalize().mult(step)));
		}
	}

	@Override
	public String toString() {
		return "Spaceship{" + "position=" + getPosition().toString() + '}';
	}
}
