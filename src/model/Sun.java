package model;

import java.util.Random;

public class Sun extends Unit {

	public Sun(int team, Point position) {
		super(team, position);
		// TODO Auto-generated constructor stub
	}

	public int health;
	public static int TOTALSPAWNCOOLDOWN = 1_000;//_000_000;
	private int spawnCooldown;
	private Random rand=new Random();

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	@Override
	public void update(double deltaT) {
		spawnCooldown -= deltaT;
		//System.out.println("spawnCooldown"+spawnCooldown);
		if (spawnCooldown <= 0) {
			spawnCooldown += TOTALSPAWNCOOLDOWN;
			produceShip();
		}

	}

	public void produceShip() {
		int radius = 35 + rand.nextInt(20);
		double angle = rand.nextDouble() * Math.PI * 2;
		Point relativeTarget = Point.fromAngle(angle).mult(radius);
		new Spaceship(getTeam(), getPosition().clone()).goToPosition(getPosition().add(relativeTarget));
	}
}
