package model;

import controller.Game;
import view.Canvas;
import view.Drawable;

public abstract class Unit {
	private Point position;
	private int team;

	public Unit(int team, Point position) {
		this.team = team;
		this.position = position;
		Game.getUnitHandler().add(this);
		Canvas.getInstance().add(new Drawable(this));
	}

	public Point getPosition() {
		return position;
	}
	
	public long getPosX() {
		return (int) position.getX();
	}
	
	public int getPosY() {
		return (int) position.getY();
	}

	public void setPosition(Point position) {
		this.position = position;
	}

	public int getTeam() {
		return team;
	}

	public void setTeam(int team) {
		this.team = team;
	}

	public abstract void update(double deltaT);
}
