package model;

public class Point {

	public double x;
	public double y;

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public Point clone() {
		return new Point(x, y);
	}

	public Point add(Point p) {
		return new Point(x + p.x, y + p.y);
	}

	public Point sub(Point p) {
		return new Point(x - p.x, y - p.y);
	}

	public Point mult(double d) {
		return new Point(x * d, y * d);
	}

	public static Point fromAngle(double a) {
		return new Point(Math.cos(a), Math.sin(a));
	}

	public Point normalize() {
		double dist = Math.hypot(x, y);
		return new Point(x / dist, y / dist);
	}
	@Override
	public String toString() {
		return "["+x+", "+y+"]";
	}
}
