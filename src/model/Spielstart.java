package model;

public class Spielstart {

	public int playerColour;
	public int map;
	
	public int getPlayerColour() {
		return playerColour;
	}
	public void setPlayerColour(int playerColour) {
		this.playerColour = playerColour;
	}
	public int getMap() {
		return map;
	}
	public void setMap(int map) {
		this.map = map;
	}
}
