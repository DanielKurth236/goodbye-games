package view;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.Game;

public class GUI extends JFrame {
	public static void main(String[] args) {
		new GUI();
	}
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 400);
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new CardLayout());
		setContentPane(contentPane);
		JPanel startscreen = new JPanel();
		contentPane.add(startscreen,"startscreen");
		startscreen.setBackground(new Color(0, 0, 51));
		startscreen.setLayout(null);
		
		
		JLabel lblNewLabel_2 = new JLabel("SpaceBox");
		lblNewLabel_2.setFont(new Font("Lucida Grande", Font.PLAIN, 30));
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setBounds(307, 76, 337, 36);
		startscreen.add(lblNewLabel_2);
		
		JButton btnNewButton = new JButton("Start");
		btnNewButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
//				 TODO Auto-generated method stub
				((CardLayout)contentPane.getLayout()).show(contentPane,"game");
				Game.getInstance().run();
			}
			
		});
		btnNewButton.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.setBounds(317, 165, 111, 44);
		startscreen.add(btnNewButton);
		//btnNewButton.addActionListener(l);
		
		JLabel lblNewLabel_1 = new JLabel("Goodbye_Games");
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setBounds(6, 258, 289, 36);
		startscreen.add(lblNewLabel_1);
		
		JPanel game = Canvas.getInstance();//.getInstance();
		contentPane.add(game,"game");
		game.setBackground(new Color(0, 0, 51));
		game.setLayout(null);
		
		JLabel lblNewLabel_3 = new JLabel("Goodbye_Games");
		lblNewLabel_3.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setBounds(6, 281, 180, 13);
		game.add(lblNewLabel_3);
		
		JPanel looser = new JPanel();
		contentPane.add(looser);
		looser.setBackground(new Color(0, 0, 51));
		looser.setLayout(null);
		
		JLabel lblNewLabel_4 = new JLabel("GAME OVER");
		lblNewLabel_4.setFont(new Font("Lucida Grande", Font.PLAIN, 30));
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setBounds(279, 70, 197, 36);
		looser.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Goodbye_Games");
		lblNewLabel_5.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		lblNewLabel_5.setForeground(Color.WHITE);
		lblNewLabel_5.setBounds(6, 281, 110, 13);
		looser.add(lblNewLabel_5);
		
		JPanel winner = new JPanel();
		contentPane.add(winner);
		winner.setBackground(new Color(0, 0, 51));
		winner.setLayout(null);
		
		JLabel lblNewLabel_6 = new JLabel("WIN");
		lblNewLabel_6.setFont(new Font("Lucida Grande", Font.PLAIN, 30));
		lblNewLabel_6.setForeground(Color.WHITE);
		lblNewLabel_6.setBounds(137, 74, 172, 36);
		winner.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Goodbye_Games");
		lblNewLabel_7.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		lblNewLabel_7.setForeground(Color.WHITE);
		lblNewLabel_7.setBounds(6, 281, 138, 13);
		winner.add(lblNewLabel_7);
		
		setVisible(true);
	}
}


