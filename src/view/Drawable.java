package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import model.Sun;
import model.Unit;

public class Drawable {
	private Image image;
	private Unit unit;
	private int unitType;
	private static final int SPACESHIP = 0, SUN = 1;
	static Image[] images;
	static Color[] teamColors;

	public Drawable(Unit unit) {
		this.unit = unit;
		if (unit instanceof Sun)
			this.unitType = SUN;
		else
			this.unitType = SPACESHIP;

		if (images == null)
			loadImages();
		if (this.unitType == SUN)
			image = images[unit.getTeam()];
	}
	//  public static void main(String[] args) {
	//	    loadImages();
	//  }

	public static void loadImages() {
		String[] names = { "sun_blue", "sun_pink", "sun_red", "sun_yellow" };
		teamColors = new Color[] { Color.blue, Color.pink, Color.red, Color.yellow };
		images = new Image[names.length];
		for (int i = 0; i < names.length; i++) {
			try {
				File f = new File("images\\"+ names[i] + ".png");// absulutePath+"sun_blue.jpeg");
				Image img = ImageIO.read(f);
				images[i] = img;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public Image getImage() {
		return image;
	};

	public final static int sunSize = 50;

	public void draw(Graphics g) {
		if (unitType == SUN)
			g.drawImage(getImage(), getX(), getY(), sunSize, sunSize, null);
		else {
			g.setColor(teamColors[unit.getTeam()]);
			g.fillRect(getX(), getY(), 2, 2);
		}
	}

	public int getX() {
		return (int) (unit.getPosition().getX() - (unitType == SUN ? (sunSize * .5) : 0));
	};

	public int getY() {
		return (int) (unit.getPosition().getY() - (unitType == SUN ? (sunSize * .5) : 0));
	}

}