package view;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import model.Point;
import model.Spaceship;
import model.Unit;

public class Canvas extends JPanel {
	List<Drawable> items = new ArrayList<>();
	private static Canvas instance;

	public static Canvas getInstance() {
		if(instance==null)
			instance = new Canvas();
		return instance;
	}

	private Canvas() {
		super();
		
	}
	public void add(Drawable d) {
		items.add(d);
	}
	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, 0, getWidth(), getHeight());
		for (int i = items.size() - 1; i >= 0; i--) 
			items.get(i).draw(g);
	}
}
