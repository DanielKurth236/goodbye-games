package controller;

import java.util.ArrayList;
import java.util.List;

import model.Point;
import model.Spaceship;
import model.Sun;
import model.Unit;
import view.Canvas;

public class Game {

	long timestamp, oldTimestamp = System.nanoTime();
	public static final double targetfps = 60.0;
	public static final double maxLoopTime = 1e9 / targetfps;
	long frames = 0;
	long timer = System.currentTimeMillis();
	private int tick = 0;
	double deltaT = 0;
	private static Game instance = new Game();

	public static Game getInstance() {
		return instance;
	}

	public static void main(String[] args) {
		getInstance().run();
	}

	private Game() {

	}

	private boolean running = false;

	public void run() {
		setup();
		new Thread(this::runImprovedGameLoop).start();
		running = true;
	}

	private void setup() {
		int[][] values = { { 1, 150, 200 }, { 0, 300, 100 }, { 0, 400, 250 }, { 0, 500, 100 }, { 2, 650, 200 } };
		for (int[] arg : values)
			new Sun(arg[0], new Point(arg[1], arg[2]));
		//new Spaceship(0, new Point(100, 100)).goToPosition(new Point(300,200));;
	}

	private void runGameLoop() {
		while (running) {
			timestamp = System.nanoTime();
			deltaT += (timestamp - oldTimestamp) / maxLoopTime;
			oldTimestamp = timestamp;
			update(deltaT);
			/*while (deltaT >= 1) {
				tick++;
				deltaT--;
				//update(deltaT, frames);

			}*/
			Canvas.getInstance().updateUI();
			frames++;

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				//System.out.println("fps: " + frames + " ticks: " + tick);
				frames = 0;
				tick = 0;
			}
		}
	}

	private void runOldGameLoop() {
		long timestamp, oldTimestamp = System.currentTimeMillis();

		while (running) {
			timestamp = System.currentTimeMillis();
			double deltaT = timestamp - oldTimestamp;
			update(deltaT);
			oldTimestamp = timestamp;
			if (deltaT < maxLoopTime)
				try {
					long sleepTime = (long) (maxLoopTime - deltaT);
					Thread.sleep(sleepTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
	}
	private void runImprovedGameLoop() {
		long timestamp, oldTimestamp = System.nanoTime();
//		int frames=0;
		while (running) {
			timestamp = System.nanoTime();
			double deltaT = timestamp - oldTimestamp;
			frames++;
//			if(timestamp/1_000_000_000!= oldTimestamp/1_000_000_000) {
//				System.out.println(frames/2);
//				System.out.println(1_000_000_000/deltaT);
//				System.out.println();
//				frames=0;
//			}
			update(deltaT/1_000_000);
			oldTimestamp = timestamp;
			if (deltaT < maxLoopTime)
				try {
					long sleepTime = (long) (maxLoopTime - deltaT);
					Thread.sleep(sleepTime/1_000_000,(int) (sleepTime%1_000_000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
	}

	private void update(double deltaT) {
		//		System.out.printf("deltaT: %f\tframes: %d\n",deltaT,frames);
		unitHandler.update(deltaT);
		Canvas.getInstance().updateUI();
	}

	private UnitHandler unitHandler = new UnitHandler();

	public static UnitHandler getUnitHandler() {
		return getInstance().unitHandler;
	}

	public class UnitHandler {
		private List<Unit> units = new ArrayList<>();

		public void add(Unit u) {
			units.add(u);
		}

		public void update(double deltaT) {
			for (int i = units.size() - 1; i >= 0; i--) {
				units.get(i).update(deltaT);
			}
		}
	}
}
